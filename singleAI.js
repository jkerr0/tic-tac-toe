function playerAI()
{
    var whoisai = whoisplayer%2+1;
    var boardCopy = isOccupied;
    var dia = [[0,4,8],[2,4,6]];
    var winflag = 0;
    var defendflag = 0;

    for(var a=0;a<3;a++)
    {
        var hor = [3*a, 3*a+1 ,3*a+2];
        var ver = [a ,  a+3,   a+2*3];   
        for(var x=0;x<3;x++)
        {
            if(boardCopy[hor[x%3]] == whoisai && boardCopy[hor[(x+1)%3]] == whoisai && boardCopy[hor[(x+2)%3]]==0) // [[boardCopy[hor[x%3]...]]] == [whoisai, whoisai, 0]
            {
                winflag = hor[(x+2)%3];
            }
            else if(boardCopy[ver[x%3]] == whoisai && boardCopy[ver[(x+1)%3]] == whoisai && boardCopy[ver[(x+2)%3]]==0)
            {
                winflag = ver[(x+2)%3];
            }
            else if(a<2 && (boardCopy[dia[a][x%3]] == whoisai && boardCopy[dia[a][(x+1)%3]] == whoisai && boardCopy[dia[a][(x+2)%3]]==0))
            {
                winflag = dia[a][(x+2)%3];
            }
            else if(boardCopy[hor[x%3]] == whoisplayer && boardCopy[hor[(x+1)%3]] == whoisplayer && boardCopy[hor[(x+2)%3]]==0) // [[boardCopy[hor[x%3]...]]] == [whoisai, whoisai, 0]
            {
                defendflag = hor[(x+2)%3];
            }
            else if(boardCopy[ver[x%3]] == whoisplayer && boardCopy[ver[(x+1)%3]] == whoisplayer && boardCopy[ver[(x+2)%3]]==0)
            {
                defendflag = ver[(x+2)%3];
            }
            else if(a<2 && (boardCopy[dia[a][x%3]] == whoisplayer && boardCopy[dia[a][(x+1)%3]] == whoisplayer && boardCopy[dia[a][(x+2)%3]]==0))
            {
                defendflag = dia[a][(x+2)%3];
            }
        }
    }
    
    if(winflag>0) return winflag;
    else if(defendflag>0) return defendflag;
    else
    {
        
        var highp=[0,2,6,8];
        var lowp=[1,3,5,7];
        if(boardCopy[4]==0) return 4;
        for(var i=0;i<4;i++)
        {
            if(boardCopy[highp[i]]==0) return highp[i];
        }
        for(var i=0;i<4;i++)
        {
            if(boardCopy[lowp[i]]==0) return lowp[i];
        }
    } 
}