

var turnCount=0;
var isOccupied=[0,0,0,0,0,0,0,0,0];
var win=0;
var whoisplayer = 0; //1x 2o
var isSingle = false;

//document.onload = startMenu();

function startMenu(mode)
{
    if(mode=="single")
    {
        console.log("Singleplayer ON");
        document.getElementById("menu").innerHTML ='<nav>Choose side: <button class="menu2" onclick="setPlayer(1)">X</button><button class="menu2" onclick="setPlayer(2)">O</button></nav>';
        isSingle = true;
        
        
    }
    else if(mode=="multi")
    {
        console.log("Singleplayer OFF");
        isSingle = false;
        whoisplayer = 1;
        
    }
    isOccupied=[0,0,0,0,0,0,0,0,0];
    turnCount = 0;
    render();
    /*if(window.confirm("Singleplayer?"))
    {
        console.log("Singleplayer ON");
        isSingle = true;
        singleplayer(true);
    }
    else 
    {
        console.log("Singleplayer OFF");
        isSingle = false;
    }*/
    
}

function setPlayer(value)
{
    console.log(whoisplayer = value);
    singleplayer(true);
}

function singleplayer(isActive)
{ 
        if(isActive && turnCount%2+1!=whoisplayer)
        {
            ihandle(playerAI());
        }
}

function ihandle(button)
{
    if(whoisplayer!=0)
    {
        if(isOccupied[button]==0 && win==0)
        {
            if(turnCount%2+1==1) isOccupied[button] = 1;
            else if(turnCount%2+1==2) isOccupied[button] = 2;
            console.log(win=checkwin());
            console.log(isOccupied);
            turnCount++;
            render();
            if(win==0 && turnCount<9) singleplayer(isSingle);
        }
    }
    else if(isSingle==true) window.alert("Please choose your side!")
    else if(isSingle==false) window.alert("Choose gamemode!")
}

function render()
{
    var who;
    if(turnCount%2+1==1) who = 'X'
    else if(turnCount%2+1==2) who = 'O';

    if(win==0 && turnCount<9) document.getElementById("turn").innerHTML = "Now " + who + " moves";
    else if(win==0 && turnCount>8) document.getElementById("turn").innerHTML = "Draw";
    else if(win==1)
    {
        document.getElementById("turn").innerHTML = "X wins";
    } 
    else if(win==2)
    {
        document.getElementById("turn").innerHTML = "O wins";
    } 
    
    for(var i=0; i<9;i++)
    {
        if(isOccupied[i])
        {
            if (isOccupied[i] == 1) display(i,'x');
            else display(i,'o');
        }
        else display(i,'');
    }
}

function checkwin()
{
    var who = turnCount%2+1; // 1x 2o who wins
    var hor;
    var ver;
    
    for(var i=0;i<3;i++)
    {
        hor = [3*i, 3*i+1 ,3*i+2];
        if(isOccupied[hor[0]]==who && isOccupied[hor[1]]==who && isOccupied[hor[2]]==who) 
        {
            markwin(hor[0],hor[1],hor[2]);
            return who;
        }
    }
    //check horizontal
    //if 3 in row occupied returns 1 if x 2 if o wins
    for(var i=0;i<3;i++)
    {
        ver = [i ,i+3, i+2*3];
        if(isOccupied[ver[0]]==who && isOccupied[ver[1]]==who &&isOccupied[ver[2]]==who)
        {   
            markwin(ver[0],ver[1],ver[2]);
            return who;
        }
    }
    //check vertical
    if(isOccupied[0]==who && isOccupied[4]==who && isOccupied[8]==who) 
    {
        markwin(0,4,8);
        return who;
    }
    else if( isOccupied[2]==who && isOccupied[4]==who && isOccupied[6]==who)
    {
        markwin(2,4,6);
        return who;
    }
    //check diagonal


    return 0;
}

function markwin(w1,w2,w3)
{
    var markcolor = "white";
    var fontcolor = "lightgreen";
    document.getElementById("b"+ w1).style.background = markcolor;
    document.getElementById("b"+ w2).style.background = markcolor;
    document.getElementById("b"+ w3).style.background = markcolor;

    document.getElementById("b"+ w1).style.color = fontcolor;
    document.getElementById("b"+ w2).style.color = fontcolor;
    document.getElementById("b"+ w3).style.color = fontcolor;
}


function display(button, fig)
{
    document.getElementById('b' + button).innerHTML = fig;
}